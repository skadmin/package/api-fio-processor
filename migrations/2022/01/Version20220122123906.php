<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220122123906 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE api_fio_processor_settings (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(255) NOT NULL, last_synchronize DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, last_update_at DATETIME DEFAULT NULL, name VARCHAR(255) NOT NULL, is_active TINYINT(1) DEFAULT \'1\' NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE api_fio_processor_transaction (id INT AUTO_INCREMENT NOT NULL, api_fio_processor_settings_id INT DEFAULT NULL, transaction_id VARCHAR(255) NOT NULL, data VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, last_update_at DATETIME DEFAULT NULL, INDEX IDX_C5A8CB75EE0B562C (api_fio_processor_settings_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE api_fio_processor_unpaired (id INT AUTO_INCREMENT NOT NULL, author VARCHAR(255) NOT NULL, amount DOUBLE PRECISION NOT NULL, currency VARCHAR(255) NOT NULL, transaction_date DATETIME NOT NULL, created_at DATETIME NOT NULL, last_update_at DATETIME DEFAULT NULL, variable_symbol VARCHAR(255) NOT NULL, note LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE api_fio_processor_transaction ADD CONSTRAINT FK_C5A8CB75EE0B562C FOREIGN KEY (api_fio_processor_settings_id) REFERENCES api_fio_processor_settings (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE api_fio_processor_transaction DROP FOREIGN KEY FK_C5A8CB75EE0B562C');
        $this->addSql('DROP TABLE api_fio_processor_settings');
        $this->addSql('DROP TABLE api_fio_processor_transaction');
        $this->addSql('DROP TABLE api_fio_processor_unpaired');
    }
}
