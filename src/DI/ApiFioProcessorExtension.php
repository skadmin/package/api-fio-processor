<?php

declare(strict_types=1);

namespace Skadmin\ApiFioProcessor\DI;

use Nette\DI\CompilerExtension;
use Nette\DI\Definitions\Statement;
use Nette\Schema\Expect;
use Nette\Schema\Schema;
use Skadmin\ApiFioProcessor\Processor\AApiFioProcessor;
use Skadmin\ApiFioProcessor\Processor\ApiFioProcessorRunner;

class ApiFioProcessorExtension extends CompilerExtension
{
    public function getConfigSchema() : Schema
    {
        return Expect::structure([
            'processors' => Expect::arrayOf(Expect::type(Statement::class)),
        ]);
    }

    public function loadConfiguration() : void
    {
        parent::loadConfiguration();

        $builder = $this->getContainerBuilder();
        $builder->addDefinition($this->prefix('apiFioProcessorRunner'))
            ->setClass(ApiFioProcessorRunner::class)
            ->setArguments([$this->config->processors]);
    }
}
