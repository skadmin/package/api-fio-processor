<?php

namespace Skadmin\ApiFioProcessor\Doctrine\ApiFioProcessorUnpaired;

use Doctrine\DBAL\Types\Types;
use SkadminUtils\DoctrineTraits\Entity;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class ApiFioProcessorUnpaired
{

    use Entity\BaseEntity;
    use Entity\VariableSymbol;
    use Entity\Note;

    #[ORM\Column]
    private string $author;

    #[ORM\Column]
    private float $amount;

    #[ORM\Column]
    private string $currency;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private DateTimeInterface $transactionDate;

    public function create(string $variableSymbol, string $author, string $note, float $amount, string $currency, DateTimeInterface $transactionDate)
    {
        $this->variableSymbol  = $variableSymbol;
        $this->author          = $author;
        $this->note            = $note;
        $this->amount          = $amount;
        $this->currency        = $currency;
        $this->transactionDate = $transactionDate;
    }

    public function getAuthor() : string
    {
        return $this->author;
    }

    public function getAmount() : float
    {
        return $this->amount;
    }
    public function getCurrency() : string
    {
        return $this->currency;
    }

    public function getTransactionDate() : DateTimeInterface
    {
        return $this->transactionDate;
    }

}
