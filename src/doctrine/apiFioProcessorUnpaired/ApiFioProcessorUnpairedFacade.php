<?php

declare(strict_types=1);

namespace Skadmin\ApiFioProcessor\Doctrine\ApiFioProcessorUnpaired;

use SkadminUtils\DoctrineTraits\Facade;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\QueryBuilder;
use Nettrine\ORM\EntityManagerDecorator;
use DateTimeInterface;

final class ApiFioProcessorUnpairedFacade extends Facade
{
    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = ApiFioProcessorUnpaired::class;
    }

    public function create(string $variableSymbol, string $author, string $note, float $amount, string $currency, DateTimeInterface $transactionDate) : ApiFioProcessorUnpaired
    {
        $apiFioProcessorUnpaired = $this->get();

        $apiFioProcessorUnpaired->create($variableSymbol, $author, $note, $amount, $currency, $transactionDate);

        $this->em->persist($apiFioProcessorUnpaired);
        $this->em->flush();

        return $apiFioProcessorUnpaired;
    }

    public function get(?int $id = null) : ApiFioProcessorUnpaired
    {
        if ($id === null) {
            return new ApiFioProcessorUnpaired();
        }

        $apiFioProcessorUnpaired = parent::get($id);

        if ($apiFioProcessorUnpaired === null) {
            return new ApiFioProcessorUnpaired();
        }

        return $apiFioProcessorUnpaired;
    }

    public function remove(int $id) : void
    {
        $apiFioProcessorUnpaired = $this->get($id);

        if ($apiFioProcessorUnpaired->isLoaded()) {
            $this->em->remove($apiFioProcessorUnpaired);
            $this->em->flush();
        }
    }

}
