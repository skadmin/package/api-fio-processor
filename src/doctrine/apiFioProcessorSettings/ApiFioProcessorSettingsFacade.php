<?php

declare(strict_types=1);

namespace Skadmin\ApiFioProcessor\Doctrine\ApiFioProcessor;

use SkadminUtils\DoctrineTraits\Facade;
use Nettrine\ORM\EntityManagerDecorator;

final class ApiFioProcessorSettingsFacade extends Facade
{
    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = ApiFioProcessorSettings::class;
    }

    public function get(?int $id = null) : ?ApiFioProcessorSettings
    {
        if ($id === null) {
            return null;
        }

        $apiFioProcessorSettings = parent::get($id);

        if ($apiFioProcessorSettings === null) {
            return null;
        }

        return $apiFioProcessorSettings;
    }

    public function findByType(string $type) : ?ApiFioProcessorSettings
    {
        $criteria = ['type' => $type, 'isActive' => true];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }

    public function updateIsActive(int $settingsId, bool $isActive) : ApiFioProcessorSettings
    {
        $settings = $this->get($settingsId);
        $settings->setIsActive($isActive);

        $this->em->persist($settings);
        $this->em->flush();

        return $settings;
    }

    public function synchronize(string $type) : ?ApiFioProcessorSettings
    {
        $criteria = ['type' => $type];

        /** @var ApiFioProcessorSettings|null $settings */
        $settings = $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);

        if (! $settings instanceof ApiFioProcessorSettings) {
            return null;
        }

        $settings->synchronize();
        $this->em->persist($settings);
        $this->em->flush();

        return $settings;
    }

}
