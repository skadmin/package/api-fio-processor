<?php

namespace Skadmin\ApiFioProcessor\Doctrine\ApiFioProcessor;

use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Nette\Utils\DateTime;
use SkadminUtils\DoctrineTraits\Entity;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class ApiFioProcessorSettings
{
    use Entity\BaseEntity;
    use Entity\Name;
    use Entity\IsActive;

    #[ORM\Column]
    private string $type;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $lastSynchronize = null;

    public function getType() : string
    {
        return $this->type;
    }

    public function isLastSynchronize() : bool
    {
        return $this->lastSynchronize instanceof DateTimeInterface;
    }

    public function getLastSynchronize() : ?DateTimeInterface
    {
        return $this->lastSynchronize;
    }

    public function synchronize() : void
    {
        $this->lastSynchronize = new DateTime();
    }

}
