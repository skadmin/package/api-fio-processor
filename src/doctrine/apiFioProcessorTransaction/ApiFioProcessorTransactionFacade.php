<?php

declare(strict_types=1);

namespace Skadmin\ApiFioProcessor\Doctrine\ApiFioProcessorTransaction;

use SkadminUtils\DoctrineTraits\Facade;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\QueryBuilder;
use FioApi\Transaction;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\ApiFioProcessor\Doctrine\ApiFioProcessor\ApiFioProcessorSettings;

final class ApiFioProcessorTransactionFacade extends Facade
{
    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = ApiFioProcessorTransaction::class;
    }

    public function create(?string $entity, ?string $entityId, string $sender, string $recipient, string $content) : ApiFioProcessorTransaction
    {
        $apiFioProcessorTransaction = $this->get();

        $apiFioProcessorTransaction->create($entity, $entityId, $sender, $recipient, $content);

        $this->em->persist($apiFioProcessorTransaction);
        $this->em->flush();

        return $apiFioProcessorTransaction;
    }

    public function get(?int $id = null) : ApiFioProcessorTransaction
    {
        if ($id === null) {
            return new ApiFioProcessorTransaction();
        }

        $apiFioProcessorTransaction = parent::get($id);

        if ($apiFioProcessorTransaction === null) {
            return new ApiFioProcessorTransaction();
        }

        return $apiFioProcessorTransaction;
    }

    public function createFromTranssaction(ApiFioProcessorSettings $settings, Transaction $transaction) : ?ApiFioProcessorTransaction
    {
        $criteria = ['apiFioProcessorSettings' => $settings, 'transactionId' => $transaction->getId()];

        $afpTransaction = $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);

        if ($afpTransaction instanceof ApiFioProcessorTransaction) {
            return null;
        }

        $afpTransaction = new ApiFioProcessorTransaction();
        $afpTransaction->create($settings, $transaction);

        $this->em->persist($afpTransaction);
        $this->em->flush();

        return $afpTransaction;
    }
}
