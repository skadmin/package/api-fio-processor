<?php

namespace Skadmin\ApiFioProcessor\Doctrine\ApiFioProcessorTransaction;

use Doctrine\DBAL\Types\Types;
use SkadminUtils\DoctrineTraits\Entity;
use FioApi\Transaction;
use Skadmin\ApiFioProcessor\Doctrine\ApiFioProcessor\ApiFioProcessorSettings;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class ApiFioProcessorTransaction
{

    use Entity\BaseEntity;

    #[ORM\Column]
    private string $transactionId;

    #[ORM\Column(type: Types::TEXT)]
    private string $data;

    #[ORM\ManyToOne(targetEntity: ApiFioProcessorSettings::class)]
    private ApiFioProcessorSettings $apiFioProcessorSettings;

    public function create(ApiFioProcessorSettings $apiFioProcessorSettings, Transaction $transaction) : void
    {
        $this->transactionId           = $transaction->getId();
        $this->apiFioProcessorSettings = $apiFioProcessorSettings;
        $this->data                    = serialize($transaction);
    }

    public function getTransactionId() : string
    {
        return $this->transactionId;
    }

    public function getApiFioProcessorSettings() : ApiFioProcessorSettings
    {
        return $this->apiFioProcessorSettings;
    }

    public function getData() : Transaction
    {
        return unserialize($this->data);
    }

}
