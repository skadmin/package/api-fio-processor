<?php

declare(strict_types=1);

namespace Skadmin\ApiFioProcessor;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE = 'api-fio-processor';

    public function getMenu() : ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fas fa-fw fa-university']),
            'items'   => ['overview', 'overview-unpaired'],
        ]);
    }
}
