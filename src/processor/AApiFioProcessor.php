<?php

namespace Skadmin\ApiFioProcessor\Processor;

use FioApi\Transaction;

abstract class AApiFioProcessor
{
    protected string $type;
    protected string $token;

    public function __construct(string $type, string $token)
    {
        $this->type  = $type;
        $this->token = $token;
    }

    public function getType() : string
    {
        return $this->type;
    }

    public function getToken() : string
    {
        return $this->token;
    }

    /** @param array<Transaction> $transactions */
    abstract public function run(array $transactions) : void;
}
