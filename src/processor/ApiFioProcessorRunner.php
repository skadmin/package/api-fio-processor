<?php

namespace Skadmin\ApiFioProcessor\Processor;

use App\Model\Override\ApiFioDownloader;
use FioApi\Transaction;
use GuzzleHttp\Client;
use Nette\Utils\DateTime;
use Skadmin\ApiFioProcessor\Doctrine\ApiFioProcessor\ApiFioProcessorSettings;
use Skadmin\ApiFioProcessor\Doctrine\ApiFioProcessor\ApiFioProcessorSettingsFacade;
use Skadmin\ApiFioProcessor\Doctrine\ApiFioProcessorTransaction\ApiFioProcessorTransactionFacade;
use Skadmin\ApiFioProcessor\Doctrine\ApiFioProcessorUnpaired\ApiFioProcessorUnpairedFacade;
use Tracy\Debugger;

class ApiFioProcessorRunner
{
    /** @var array<AApiFioProcessor> */
    private $processors = [];

    private ApiFioProcessorSettingsFacade    $facadeApiFioProcessorSettings;
    private ApiFioProcessorTransactionFacade $facadeApiFioProcessorTransaction;

    /**
     * @param AApiFioProcessor[] $processors
     */
    public function __construct(array $processors, ApiFioProcessorSettingsFacade $facadeApiFioProcessorSettings, ApiFioProcessorTransactionFacade $facadeApiFioProcessorTransaction)
    {
        $this->processors                       = $processors;
        $this->facadeApiFioProcessorSettings    = $facadeApiFioProcessorSettings;
        $this->facadeApiFioProcessorTransaction = $facadeApiFioProcessorTransaction;
    }

    public function run() : void
    {
        foreach ($this->processors as $processor) {
            $settings = $this->facadeApiFioProcessorSettings->findByType($processor->getType());

            if ($settings instanceof ApiFioProcessorSettings) {
                $processor->run($this->prepareTransaction($settings, $processor->getToken()));
            }
        }
    }

    /** @return array<Transaction> */
    private function prepareTransaction(ApiFioProcessorSettings $settings, string $token) : array
    {
        if ($settings->isLastSynchronize()) {
            $from = $settings->getLastSynchronize();
        } else {
            $from = (new DateTime())->modify('-1 months');
        }

        $apiFioDownloader = new ApiFioDownloader($token, new Client(['verify' => false]));

        $transactions    = [];
        $transactionList = $apiFioDownloader->downloadSince($from);
        foreach ($transactionList->getTransactions() as $transaction) {
            if ($this->facadeApiFioProcessorTransaction->createFromTranssaction($settings, $transaction)) {
                $transactions[] = $transaction;
            }
        }

        $this->facadeApiFioProcessorSettings->synchronize($settings->getType());

        return $transactions;
    }


}
