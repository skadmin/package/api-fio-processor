<?php

declare(strict_types=1);

namespace Skadmin\ApiFioProcessor\Components\Admin;

use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Skadmin\ApiFioProcessor\BaseControl;
use Skadmin\ApiFioProcessor\Doctrine\ApiFioProcessorUnpaired\ApiFioProcessorUnpaired;
use Skadmin\ApiFioProcessor\Doctrine\ApiFioProcessorUnpaired\ApiFioProcessorUnpairedFacade;
use Skadmin\Translator\Translator;
use Ublaboo\DataGrid\Column\Action\Confirmation\StringConfirmation;

class OverviewUnpaired extends GridControl
{
    use APackageControl;

    private ApiFioProcessorUnpairedFacade $facade;

    public function __construct(ApiFioProcessorUnpairedFacade $facade, Translator $translator, User $user)
    {
        parent::__construct($translator, $user);

        $this->facade = $facade;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null)
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overviewUnpaired.latte');
        $template->drawBox = $this->drawBox;
        $template->render();
    }

    public function getTitle() : string
    {
        return 'api-fio-processor.overview-unpaired.title';
    }

    protected function createComponentGrid(string $name) : GridDoctrine
    {
        // GRID
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel());

        // COLUMNS
        $grid->addColumnText('variableSymbol', 'grid.api-fio-processor.overview-unpaired.variable-symbol');
        $grid->addColumnText('author', 'grid.api-fio-processor.overview-unpaired.author');
        $grid->addColumnText('amount', 'grid.api-fio-processor.overview-unpaired.amount')
            ->setRenderer(fn(ApiFioProcessorUnpaired $afpUnpaired) : string => number_format($afpUnpaired->getAmount(), 2, ',', ' '))
            ->setAlign('right');
        $grid->addColumnText('currency', 'grid.api-fio-processor.overview-unpaired.currency');
        $grid->addColumnText('note', 'grid.api-fio-processor.overview-unpaired.note');
        $grid->addColumnDateTime('transactionDate', 'grid.api-fio-processor.overview-unpaired.transaction-date')
            ->setFormat('d.m.Y H:i')
            ->setAlign('center');

        // FILTER
        $grid->addFilterText('variableSymbol', 'grid.api-fio-processor.overview-unpaired.variable-symbol');
        $grid->addFilterText('author', 'grid.api-fio-processor.overview-unpaired.author');
        $grid->addFilterText('amount', 'grid.api-fio-processor.overview-unpaired.amount');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $grid->addActionCallback('remove', 'grid.api-fio-processor.overview-unpaired.action.remove', [$this, 'gridOverviewUnpairedRemove'])
                ->setConfirmation(new StringConfirmation('grid.api-fio-processor.overview-unpaired.action.remove.confirm %%s', 'variableSymbol'))
                ->setClass('btn btn-xs btn-danger ajax')
                ->setIcon('trash');
        }

        // OTHER
        $grid->setDefaultSort(['transactionDate' => 'DESC']);

        return $grid;
    }

    public function gridOverviewUnpairedRemove(string $id) : void
    {
        $presenter = $this->getPresenterIfExists();

        $this->facade->remove(intval($id));
        if ($presenter !== null) {
            $presenter->flashMessage('grid.api-fio-processor.overview-unpaired.flash.remove.success', Flash::SUCCESS);
        }

        $this['grid']->reload();
    }
}
