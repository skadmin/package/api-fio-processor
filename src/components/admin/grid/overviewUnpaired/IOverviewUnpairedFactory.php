<?php

declare(strict_types=1);

namespace Skadmin\ApiFioProcessor\Components\Admin;

interface IOverviewUnpairedFactory
{
    public function create() : OverviewUnpaired;
}
