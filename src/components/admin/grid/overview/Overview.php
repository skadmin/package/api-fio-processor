<?php

declare(strict_types=1);

namespace Skadmin\ApiFioProcessor\Components\Admin;

use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\Grid\Traits\IsActive;
use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Skadmin\ApiFioProcessor\BaseControl;
use Skadmin\ApiFioProcessor\Doctrine\ApiFioProcessor\ApiFioProcessorSettingsFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;

/**
 * Class Overview
 */
class Overview extends GridControl
{
    use APackageControl;
    use IsActive;

    private ApiFioProcessorSettingsFacade $facade;

    public function __construct(ApiFioProcessorSettingsFacade $facade, Translator $translator, User $user)
    {
        parent::__construct($translator, $user);

        $this->facade = $facade;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null)
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle() : string
    {
        return 'api-fio-processor.overview.title';
    }

    protected function createComponentGrid(string $name) : GridDoctrine
    {
        // GRID
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel());

        // COLUMNS
        $grid->addColumnText('name', 'grid.api-fio-processor.overview.name');
        $grid->addColumnDateTime('lastSynchronize', 'grid.api-fio-processor.overview.last-synchronize')
            ->setFormat('d.m.Y H:i')
            ->setAlign('center');

        $grid->addColumnStatus('isActive', 'grid.api-fio-processor.overview.is-active')
            ->setAlign('center')
            ->addOption(true, $this->getReplacementIsActive()[1])
            ->setClass('btn-primary')
            ->setIcon('check')
            ->endOption()
            ->addOption(false, $this->getReplacementIsActive()[0])
            ->setClass('btn-danger')
            ->setIcon('times')
            ->endOption()
            ->onChange[] = [$this, 'overviewRegistrationOnChangeStatus'];

        // FILTER
        $grid->addFilterText('name', 'grid.api-fio-processor.overview.name');
        $this->addFilterIsActive($grid, 'api-fio-processor.overview');

        // OTHER
        $grid->setDefaultSort(['name' => 'ASC']);

        return $grid;
    }

    public function overviewRegistrationOnChangeStatus(string $id, string $isActive) : void
    {
        $id       = intval($id);
        $isActive = boolval(intval($isActive));

        $settings = $this->facade->updateIsActive($id, $isActive);

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null) {
            $message = new SimpleTranslation('grid.api-fio-processor.overview.flash.update-is-active %s', [$settings->getName()]);
            $presenter->flashMessage($message, Flash::SUCCESS);
        }

        $this['grid']->redrawItem($id);
    }
}
